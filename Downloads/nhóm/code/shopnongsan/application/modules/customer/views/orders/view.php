<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Đặt hàng #<?php echo $data->order_number; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><?php echo anchor('customer', 'Home'); ?></li>
                        <li class="breadcrumb-item active"><?php echo anchor('customer/orders', 'Order'); ?></li>
                        <li class="breadcrumb-item active">#<?php echo $data->order_number; ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-8">
                <div class="card card-primary">
                    <div class="card-header">
                        <h5 class="card-heading">Dữ liệu đặt hàng</h5>
                    </div>
                    <div class="card-body p-0">
                        <table class="table table-hover table-striped table-hover">
                            <tr>
                                <td>Số</td>
                                <td><b>#<?php echo $data->order_number; ?></b></td>
                            </tr>
                            <tr>
                                <td>Ngày</td>
                                <td><b><?php echo get_formatted_date($data->order_date); ?></b></td>
                            </tr>
                            <tr>
                                <td>Item</td>
                                <td><b><?php echo $data->total_items; ?></b></td>
                            </tr>
                            <tr>
                                <td>Giá bán</td>
                                <td><b> <?php echo format_rupiah($data->total_price); ?></b></td>
                            </tr>
                            <tr>
                                <td>Phương thức thanh toán</td>
                                <td><b><?php echo ($data->payment_method == 1) ? 'Transfer bank' : 'Thanh toán tại chỗ
                                '; ?></b></td>
                            </tr>
                            <tr>
                                <td>Trạng thái</td>
                                <td><b class="statusField"><?php echo get_order_status($data->order_status, $data->payment_method); ?></b></td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div class="card card-primary">
                    <div class="card-header">
                        <h5 class="card-heading">Hàng hóa theo đơn đặt hàng</h5>
                    </div>
                    <div class="card-body p-0">
                        <table class="table table-hover table-condensed">
                            <tr>
                                <th scope="col"></th>
                                <th scope="col">Sản phẩm</th>
                                <th scope="col">Số tiền đã mua</th>
                                <th scope="col">Đơn giá</th>
                            </tr>
                          <?php foreach ($items as $item) : ?>
                            <tr>
                                <td>
                                    <img class="img img-fluid rounded" style="width: 60px; height: 60px;" alt="<?php echo $item->name; ?>" src="<?php echo base_url('assets/uploads/products/'. $item->picture_name); ?>">
                                </td>
                                <td>
                                    <h5 class="mb-0"><?php echo $item->name; ?></h5>
                                </td>
                                <td><?php echo $item->order_qty; ?></td>
                                <td> <?php echo format_rupiah($item->order_price); ?></td>
                            </tr>
                          <?php endforeach; ?>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card card-primary">
                    <div class="card-header">
                        <h5 class="card-heading">Dữ liệu người nhận</h5>
                    </div>
                    <div class="card-body p-0">
                        <table class="table table-hover table-striped table-hover">
                            <tr>
                                <td>Tên</td>
                                <td><b><?php echo $delivery_data->customer->name; ?></b></td>
                            </tr>
                            <tr>
                                <td>Phone</td>
                                <td><b><?php echo $delivery_data->customer->phone_number; ?></b></td>
                            </tr>
                            <tr>
                                <td>Địa chỉ</td>
                                <td><b><?php echo $delivery_data->customer->address; ?></b></td>
                            </tr>
                            <tr>
                                <td>Ghi chú</td>
                                <td><b><?php echo $delivery_data->note; ?></b></td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div class="card card-primary">
                    <div class="card-header">
                        <h5 class="card-heading">Thanh toán</h5>
                    </div>
                    <div class="card-body p-0">
                      <?php if ($data->payment_price == NULL) : ?>
                      <div class="alert alert-info m-2">Không có dữ liệu thanh toán.</div>
                      <?php else : ?>
                        <table class="table table-hover table-striped table-hover">
                            <tr>
                                <td>chuyển khoản</td>
                                <td><b> <?php echo format_rupiah($data->payment_price); ?></b></td>
                            </tr>
                            <tr>
                                <td>Ngày</td>
                                <td><b><?php echo get_formatted_date($data->payment_date); ?></b></td>
                            </tr>
                            <tr>
                                <td>Trạng thái</td>
                                <td><b>
                                  <?php if ($data->payment_status == 1) : ?>
                                    <span class="badge badge-warning text-white">chờ đợi sự xác nhận</span>
                                  <?php elseif ($data->payment_method == 2) : ?>
                                    <span class="badge badge-success text-white">Đã xác nhận</span>
                                  <?php elseif ($data->payment_method == 3) : ?>
                                    <span class="badge badge-danger text-white">Thất bại</span>
                                  <?php endif; ?>
                                </b></td>
                            </tr>
                            <tr>
                                <td>Chuyển đến</td>
                                <td><b>
                                    <?php
                                        $bank_data = json_decode($data->payment_data);
                                        $bank_data  = (Array) $bank_data;
                                        $transfer_to = $bank_data['transfer_to'];

                                        $transfer_to = $banks[$transfer_to];
                                        $transfer_from = $bank_data['source'];
                                    ?>
                                    <?php echo $transfer_to->bank; ?> a.n <?php echo $transfer_to->name; ?> (<?php echo $transfer_to->number; ?>)
                                </b></td>
                            </tr>
                            <tr>
                                <td>Chuyển từ</td>
                                <td><b><?php echo $transfer_from->bank; ?> a.n <?php echo $transfer_from->name; ?> (<?php echo $transfer_from->number; ?>)</b></td>
                            </tr>
                        </table>
                      <?php endif; ?>
                    </div>
                </div>

                <div class="card card-primary">
                    <div class="card-header">
                        <h5 class="card-heading">Hoạt động</h5>
                    </div>
                    <div class="card-body">
                        <div class="text-center actionRow">
                          <?php if ($data->payment_method == 1) : ?>
                            <?php if ($data->order_status == 1) : ?>
                                <p>Đơn hàng đang chờ thanh toán</p>
                                <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#cancelModal"><i class="fa fa-times"></i> Huỷ bỏ</a>
                            <?php elseif ($data->order_status == 5) : ?>
                                <p>Lệnh hủy</p>
                                <a href="#" class="btn btn-warning" data-toggle="modal" data-target="#deleteModal"><i class="fa fa-trash"></i> xóa bỏ</a>
                            <?php elseif ($data->order_status == 2) : ?>
                                <p>Đơn hàng đang được thực hiện</p>
                            <?php elseif ($data->order_status == 3) : ?>
                                <p>Đang vận chuyển</p>
                            <?php elseif ($data->order_status == 4) : ?>
                                <p>Đặt hàng thành công</p>
                                <a href="#" class="btn btn-warning" data-toggle="modal" data-target="#deleteModal"><i class="fa fa-trash"></i> xóa bỏ</a>
                            <?php endif; ?>
                          <?php elseif ($data->payment_method == 2) : ?>
                            <?php if ($data->order_status == 1) : ?>
                                <p>Đơn hàng đang được thực hiện</p>
                                <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#cancelModal"><i class="fa fa-times"></i> Huỷ bỏ</a>
                            <?php elseif ($data->order_status == 4) : ?>
                                <p>Đặt hàng thành công</p>
                                <a href="#" class="btn btn-warning" data-toggle="modal" data-target="#deleteModal"><i class="fa fa-trash"></i> xóa bỏ</a>
                            <?php elseif ($data->order_status == 2) : ?>
                                <p>Đang vận chuyển</p>
                            <?php elseif ($data->order_status == 3) : ?>
                                <p>Đặt hàng thành công</p>
                                <a href="#" class="btn btn-warning" data-toggle="modal" data-target="#deleteModal"><i class="fa fa-trash"></i> xóa bỏ</a>
                            <?php endif; ?>
                          <?php endif; ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

</div>

<?php if ( ($data->payment_method == 1 && $data->order_status == 1) || ($data->payment_method == 2 && $data->order_status == 1)) : ?>
<div class="modal fade" id="cancelModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="cancelModalLabel">Hủy đơn hàng</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Bạn có chắc chắn muốn hủy đơn đặt hàng không? Vui lòng liên hệ với chúng tôi để được hoàn lại tiền.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger cancel-btn">Huỷ bỏ</button>
      </div>
    </div>
  </div>
</div>

<script>
$('.cancel-btn').click(function(e) {
    e.preventDefault();

    $(this).html('<i class="fa fa-spin fa-spinner"></i> Huỷ bỏ...');
    
    $.ajax({
        method: 'POST',
        url: '<?php echo site_url('customer/orders/order_api?action=cancel_order'); ?>',
        data: { id: <?php echo $data->id; ?>},
        context: this,
        success: function (res) {
            if (res.code == 200) {
                $(this).html('Huỷ bỏ');

                if (res.success) {
                    $('.statusField').text('Đã hủy');
                    $('.actionRow').html('Lệnh hủy');
                }
                else if (res.error) {
                    $('.actionRow').html(res.message);
                }

                setTimeout(() => {
                    $('#cancelModal').modal('hide');
                }, 2000);
            }
        }
    })
})
</script>
<?php endif; ?>

<?php if ( ($data->payment_method == 1 && ($data->order_status == 5 || $data->order_status == 4)) || ($data->payment_method == 2 && ($data->order_status == 4 || $data->order_status == 3))) : ?>
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deletelModalLabel">Xóa đơn hàng</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p class="deleteText">Bạn có chắc chắn muốn xóa đơn đặt hàng không? Tất cả dữ liệu liên quan cũng sẽ bị xóa</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning delete-btn">xóa bỏ</button>
      </div>
    </div>
  </div>
</div>

<script>
$('.delete-btn').click(function(e) {
    e.preventDefault();

    $(this).html('<i class="fa fa-spin fa-spinner"></i> Menghapus...');
    var del = $('.deleteText');
    
    $.ajax({
        method: 'POST',
        url: '<?php echo site_url('customer/orders/order_api?action=delete_order'); ?>',
        data: { id: <?php echo $data->id; ?>},
        context: this,
        success: function (res) {
            if (res.code == 200) {
                $(this).html('Hapus');

                if (res.success) {
                    del.html('Đơn đặt hàng và tất cả dữ liệu đã được xóa thành công');

                    setTimeout(() => {
                        del.html('<i class="fa fa-spin fa-spinner"></i> Mengalihkan...');
                    }, 3000);
                    setTimeout(() => {
                        window.location = '<?php echo site_url('customer/orders'); ?>';
                    }, 5000);
                }
                else if (res.error) {
                    $('.actionRow').html(res.message);

                    setTimeout(() => {
                        $('#deleteModal').modal('hide');
                    }, 2000);
                }
            }
        }
    })
})
</script>
<?php endif; ?>