<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Phản hồi đặt hàng #<?php echo $review->order_number; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><?php echo anchor(base_url(), 'Home'); ?></li>
                        <li class="breadcrumb-item"><?php echo anchor('customer/reviews', 'Review'); ?></li>
                        <li class="breadcrumb-item active">Order #<?php echo $review->order_number; ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
     <div class="row">
      <div class="col-md-8">
        <div class="card card-primary">
            <div class="card-body p-0">
                <table class="table table-hover table-striped">
                    <tr>
                        <td>Tiêu đề</td>
                        <td><b><?php echo $review->title; ?></b></td>
                    </tr>
                    <tr>
                        <td>Đặt hàng</td>
                        <td><b>#<?php echo $review->order_number; ?></b></td>
                    </tr>
                    <tr>
                        <td>Ngày</td>
                        <td><b><?php echo get_formatted_date($review->review_date); ?></b></td>
                    </tr>
                    <tr>
                        <td>Phản hồi</td>
                        <td><b><?php echo $review->review_text; ?></b></td>
                    </tr>
                </table>
            </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="text-center">
            <a href="#" data-toggle="modal" data-target="#deleteModal" class="btn btn-danger">
                <i class="fa fa-trash"></i> Xóa bỏ
            </a>
        </div>
      </div>
     </div>
    </section>

</div>

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deletelModalLabel">Xóa bài đánh giá</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p class="deleteText">Bạn có chắc chắn muốn xóa bài đánh giá?</p>
      </div>
      <div class="modal-footer">
      <?php echo anchor('customer/reviews/delete/'. $review->id, 'Hapus', array('class' => 'btn btn-danger')); ?>
      </div>
    </div>
  </div>
</div>