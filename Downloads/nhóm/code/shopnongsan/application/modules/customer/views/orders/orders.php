<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1> Đơn hàng của tôi</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><?php echo anchor(base_url(), 'Home'); ?></li>
                        <li class="breadcrumb-item active">Đặt hàng</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="card card-primary">
            <div class="card-body<?php echo ( count($orders) > 0) ? ' p-0' : ''; ?>">
            <?php if ( count($orders) > 0) : ?>
                <div class="table-responsive">
                    <table class="table table-striped m-0">
                        <tr class="bg-primary">
                            <th scope="col">Phone</th>
                            <th scope="col">ID</th>
                            <th scope="col">Ngày</th>
                            <th scope="col">số lượng đặt hàng</th>
                            <th scope="col">Tổng số đơn hàng</th>
                            <th scope="col">Thanh toán</th>
                            <th scope="col">Trạng thái</th>
                        </tr>
                        <?php foreach ($orders as $order) : ?>
                        <tr>
                            <td><?php echo $order->id; ?></td>
                            <td><?php echo anchor('customer/orders/view/'. $order->id, '#'. $order->order_number); ?></td>
                            <td><?php echo get_formatted_date($order->order_date); ?></td>
                            <td><?php echo $order->total_items; ?> barang</td>
                            <td>Rp <?php echo format_rupiah($order->total_price); ?></td>
                            <td><?php echo ($order->payment_method == 1) ? 'Transfer bank' : 'Thanh toán ngay tại chỗ'; ?></td>
                            <td><?php echo get_order_status($order->order_status, $order->payment_method); ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php else : ?>
                <div class="row">
                    <div class="col-md-6">
                        <div class="alert alert-info">
                            Không có dữ liệu đặt hàng được nêu ra.
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            </div>

            <?php if ($pagination) : ?>
            <div class="card-footer">
                <?php echo $pagination; ?> 
            </div>
            <?php endif; ?>

        </div>
    </section>

</div>